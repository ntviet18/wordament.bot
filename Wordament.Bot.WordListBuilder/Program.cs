﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Wordament.Bot.WordListBuilder
{
    class Program
    {
        private static int MIN_LISTS = 2;
        private static Regex nonAlphaRegex = new Regex("[^a-zA-Z]");

        static void Main(string[] args)
        {
            var dir = Environment.CurrentDirectory + "\\Lists";
            var files = Directory.EnumerateFiles(dir);

            ISet<string> finalList = new HashSet<string>();
            Dictionary<string, int> wordHits = new Dictionary<string, int>();
            foreach (var file in files)
            {
                var list = File.ReadAllLines(file).Select(line => Clean(line));
                foreach (var word in list)
                {
                    if (word.Length < 3) continue;
                    if (wordHits.ContainsKey(word))
                    {
                        wordHits[word] = wordHits[word] + 1;
                    }
                    else
                    {
                        wordHits.Add(word, 1);
                    }
                    if (wordHits[word] == MIN_LISTS)
                    {
                        finalList.Add(word);
                    }
                }
            }
            foreach (var word in finalList)
            {
                Console.WriteLine(word);
            }
            Console.WriteLine(finalList.Count);
            Console.ReadKey();
        }
        private static string Clean(string line)
        {
            line = nonAlphaRegex.Replace(line, "");
            return line.ToUpperInvariant();
        }
    }
}
