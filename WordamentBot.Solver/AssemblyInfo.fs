﻿namespace WordamentBot.Solver.AssemblyInfo

open System.Reflection
open System.Runtime.CompilerServices
open System.Runtime.InteropServices

[<assembly: AssemblyTitle("WordamentBot.Solver")>]
[<assembly: AssemblyCompany("Martin Doms")>]
[<assembly: AssemblyProduct("WordamentBot.Solver")>]
[<assembly: AssemblyCopyright("Copyright ©  2016")>]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[<assembly: ComVisible(false)>]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[<assembly: Guid("28e7610e-4ec5-42da-85e7-b092204efda3")>]

do
    ()