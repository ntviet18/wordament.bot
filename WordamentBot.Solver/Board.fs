﻿[<RequireQualifiedAccess>]
module Board

let cols = 4
let rows = 4
let cells = rows * cols

type Solution(word, positions) =
    member this.word = word
    member this.positions = positions

type Letter(index, character, neighbours) =
    member this.character = character
    member this.neighbours = neighbours
    member this.index = index

let generateNeighbours board currentIndex =
    seq { 
        if currentIndex > cols then
            if currentIndex % cols > 0 then yield currentIndex-(cols+1)
            yield currentIndex-cols
            if currentIndex % cols < (cols-1) then yield currentIndex-(cols-1)
        if currentIndex % cols > 0 then yield currentIndex-1
        if currentIndex % cols < (cols-1) then yield currentIndex+1
        if currentIndex < cells-cols then
            if currentIndex % cols > 0 then yield currentIndex+(cols-1)
            yield currentIndex + cols;
            if currentIndex % cols < (cols-1) then yield currentIndex+(cols+1)
    }

type Board(letters:string) =  
    let charList = List.ofArray(letters.ToCharArray())
    member this.letterGraph = 
        charList
        |> List.mapi (fun index character -> 
            new Letter(index, character, (List.ofSeq (generateNeighbours letters index))))

let depthFirstSearch (board:Board) (shortCircuitFunction:string -> bool) (selectorFunction:string -> bool) =
    let rec search (board:Board) (currNode:Letter) currString path searchedList =
        seq {
            let newString = currString + currNode.character.ToString()
            if not(shortCircuitFunction newString) then
                if selectorFunction newString then
                    yield Solution(newString, List.rev(path))
                for neighbourIndex in currNode.neighbours do
                    if not(path |> List.contains neighbourIndex) then 
                        let nextLetter = board.letterGraph.[neighbourIndex]
                        yield! search board nextLetter newString (nextLetter.index :: path) (currNode.index :: searchedList)
        }
    seq {
        for letter in board.letterGraph do
            yield! search board letter "" [letter.index] []
    }

