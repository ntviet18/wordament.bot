﻿[<RequireQualifiedAccess>]
module Wordament.Bot.Solver

open System.Collections.Generic;
open System.IO;
open System.Text;
open System.Text.RegularExpressions;

let isLetter l =
    l>='A' && l<='Z'

let injest stream =
    let regex = new Regex("[^a-zA-Z]")
    let lines = System.IO.File.ReadAllLines stream
                |> Seq.map (fun l -> l.ToUpperInvariant())
                |> Seq.filter(fun l -> l.Length > 2)
                |> Seq.map (fun w -> regex.Replace(w, ""))
                |> Seq.filter (fun f -> f.Length > 2)
                |> List.ofSeq

    new Trie.Trie(lines)

let solve boardLetters trie =
    let board = new Board.Board(boardLetters)
    let solutions = Board.depthFirstSearch board (fun s -> not(Trie.isPrefix(trie, s))) (fun s -> Trie.isWord(trie, s))
                   |> Seq.filter(fun s -> s.word.Length > 2)
                   |> Seq.distinctBy(fun s -> s.word)
                   |> Seq.sortBy(fun s -> s.word)
                   |> Seq.sortByDescending(fun s -> s.word.Length)
    solutions

let loadTrie dictionaryStream =
    let trie = injest dictionaryStream
    trie

