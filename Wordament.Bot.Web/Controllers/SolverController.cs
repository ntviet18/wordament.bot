﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Wordament.Bot.Web.Controllers
{
    public class SolverController : Controller
    {
        private static Trie.Trie trie;

        static SolverController()
        {
            trie = Solver.loadTrie(HostingEnvironment.MapPath("~/App_Data/list.txt"));
        }
        

        [System.Web.Http.HttpPost]
        public ActionResult Index()
        {
            var bitmap = (Bitmap)Image.FromStream(Request.InputStream);
            var letters = Reader.read(bitmap);

            var result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = (Solver.solve(string.Concat(letters), trie));
            return result;
        }
    }
}