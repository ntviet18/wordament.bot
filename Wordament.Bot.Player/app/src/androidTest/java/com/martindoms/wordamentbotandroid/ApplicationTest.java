package com.martindoms.wordamentbotandroid;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.StaleObjectException;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject2;
import android.test.suitebuilder.annotation.SmallTest;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class ApplicationTest {

    private UiDevice device;

    @Before
    public void before() {
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
    }

    @Test
    public void start() {
        while (true) {
            try {
                play();
            }
            catch (Exception e) {
                // and round and round it goes
            }
        }
    }

    private void play() {
        try {
            String dir = Environment
                    .getExternalStorageDirectory()
                    .getAbsolutePath();

            String filename = "file.png";
            File file = new File(dir + File.separator + filename);

            UiObject2 boardView = getGameBoard();

            Log.d("Wordament.bot", "Getting tiles and screenshotting board");
            List<Point> centers = getTileCoordinates(boardView);

            boolean screenshotResult = device.takeScreenshot(file, 0.1f, 20);
            if (!screenshotResult) {
                Log.d("Wordament.bot", "Failed to take screenshot");
            }
            else {
                Solution[] solutions = getSolutionsFromWeb(file);
                Log.d("Wordament.bot", "Found " + solutions.length + " possible solutions...");


                for (Solution solution: solutions) {
                    solution.solve(device, centers);
                    Log.d("Wordament.bot", "Swiped + " + solution.word);
                }
            }
        }
        catch (StaleObjectException e) {
            // end of the match
        }

    }

    @NonNull
    private List<Point> getTileCoordinates(UiObject2 board) {
        List<UiObject2> tiles = board.getChildren();
        List<Point> centers = new ArrayList<>();
        for (int i = 0; i < tiles.size(); i++) {
            centers.add(tiles.get(i).getVisibleCenter());
        }
        return centers;
    }

    @NonNull
    private UiObject2 getGameBoard() {
        UiObject2 boardView = null;
        Log.d("Wordament.bot", "Waiting for device");
        do {
            device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
            try {
                List<UiObject2> objects = device.findObjects(By.clazz("android.widget.RelativeLayout"));
                for (UiObject2 view : objects) {

                    // the game board is a view with exactly 16 child views (the letter tiles)
                    // and takes up at least two thirds of the screen (so it's not the half-screen
                    // game board you see between rounds).
                    if (view.getChildCount() == 16 &&
                        view.getVisibleBounds().width() > ((float)device.getDisplayWidth())/1.5f) {
                        Log.d("Wordament.bot", "Found view with 16 subviews");
                        boardView = view;
                        break;
                    }
                }
            } catch (StaleObjectException e) {
                Log.d("Wordament.bot", "Caught StaleObjectException, probably changing screens");
            }
        } while (boardView == null); // if the board is not visible, go back and try again
        return boardView;
    }

    @NonNull
    private Solution[] getSolutionsFromWeb(File file) {

        try {
            String dir = Environment
                    .getExternalStorageDirectory()
                    .getAbsolutePath();

            File compressedFile = new File(dir + File.separator + "file2.png");

            Bitmap capturedImage = BitmapFactory.decodeFile(file.getPath());
            Bitmap bitmap = Bitmap.createScaledBitmap(capturedImage, capturedImage.getWidth()/4, capturedImage.getHeight()/4, true);
            FileOutputStream fos = new FileOutputStream(compressedFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 20, fos);
            fos.flush();
            fos.close();
            byte[] bytes = IOUtils.toByteArray(new FileInputStream(file));
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url("http://wordamentbot.azurewebsites.net/solver")
                    .method("POST", RequestBody.create(
                            okhttp3.MediaType.parse("image/png"),
                            bytes))
                    .build();

            Response response = client.newCall(request).execute();
            Gson gson = new Gson();
            return gson.fromJson(response.body().string(), Solution[].class);
        } catch (IOException e) {
            Log.d("Wordament.bot", "Problem contacting OCR service\r\n" + e.getMessage());
            return new Solution[0];
        }
    }

    class Solution {
        public String word;
        public int[] positions;

        // swipe this solution onto the given device with supplied letter tile coordinates
        protected void solve(UiDevice device, List<Point> tileCoords) {
            Point[] coords = new Point[this.positions.length];
            for (int i = 0; i < this.positions.length; i++) {
                int index = this.positions[i];
                coords[i] = tileCoords.get(index);
            }
            device.swipe(coords, coords.length);
        }
    }
}